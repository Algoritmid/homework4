

import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   //private double a1, b1, c1, d1;
   private double a, b, c, d;

   // TODO!!! Your fields here!

   /**
    * Constructor from four double values.
    *
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion(double a, double b, double c, double d) {
      //a1 = a;
      //b1 = b;
      //c1 = c;
      //d1 = d;
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;

      // TODO!!! Your constructor here!
   }

   /**
    * Real part of the quaternion.
    *
    * @return real part
    */
   public double getRpart() {
      return a; // TODO!!!
   }

   /**
    * Imaginary part i of the quaternion.
    *
    * @return imaginary part i
    */
   public double getIpart() {
      return b; // TODO!!!
   }

   /**
    * Imaginary part j of the quaternion.
    *
    * @return imaginary part j
    */
   public double getJpart() {
      return c; // TODO!!!
   }

   /**
    * Imaginary part k of the quaternion.
    *
    * @return imaginary part k
    */
   public double getKpart() {
      return d; // TODO!!!
   }

   /**
    * Conversion of the quaternion to the string.
    *
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      return a + "+" + b + "i" + "+" + c + "j" + "+" + d + "k"; // TODO!!!
   }

   /**
    * Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    *
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    * @throws IllegalArgumentException if string s does not represent
    *                                  a quaternion (defined by the <code>toString</code> method)
    */
   public static Quaternion valueOf(String s) {
      //KAsutatud Joonatan Uusv�li lahendust
      //String[] nums = s.split("\\++"); //http://javadevnotes.com/java-string-split-tutorial-and-examples
      String[] nums = s.split("\\+");
      //System.out.println("nums============="+Arrays.toString(nums));
      if (nums.length != 4) {
         throw new IllegalArgumentException("Formaat peab olema: x + xi + xj + xk, mitte nagu teie sisestatud kuju: "+s);
      }else {
         if(!nums[1].contains("i")){throw new IllegalArgumentException("teil on avaldises imagigaal osa puudu (oige kuju x + xi + xj + xk), teil avaldis: "+s);}
         else if(!nums[2].contains("j")){throw new IllegalArgumentException("teil on avaldises imagigaal osa puudu (oige kuju x + xi + xj + xk), teil avaldis: "+s);}
         else if(!nums[3].contains("k")){throw new IllegalArgumentException("teil on avaldises imagigaal osa puudu (oige kuju x + xi + xj + xk), teil avaldis: "+s);}
         else
            try {
               Double.parseDouble(nums[0]);
            } catch (NumberFormatException e){
               throw new NumberFormatException("Viga esimeses numbris, teil aga; "+s);
            }
         try {
            Double.parseDouble(nums[1].substring(0, nums[1].length() - 1)); //Double.parseDouble(nums[1].replace("i", ""));
         } catch (NumberFormatException e){
            throw new NumberFormatException("Viga teises numbris, teil aga; "+s);            }
         try {
            Double.parseDouble(nums[2].substring(0, nums[2].length() - 1));//Double.parseDouble(nums[2].replace("j", ""));
         } catch (NumberFormatException e){
            throw new NumberFormatException("Viga kolmandas numbris, teil aga; "+s);            }
         try {
            Double.parseDouble(nums[3].substring(0, nums[3].length() - 1));//Double.parseDouble(ss[3].replace("k", ""));
         } catch (NumberFormatException e){
            throw new NumberFormatException("Viga neljandas numbris, teil aga; "+s);
         }
      }

      return new Quaternion( //votab ara imaginaar osade tahised i, j, k
              Double.parseDouble(nums[0]), // parseDouble teeb nt stringi doubleks
              Double.parseDouble(nums[1].substring(0, nums[1].length() - 1)),
              Double.parseDouble(nums[2].substring(0, nums[2].length() - 1)),
              Double.parseDouble(nums[3].substring(0, nums[3].length() - 1))
      ); // TODO!!!
   }

   /**
    * Clone of the quaternion.
    *
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Quaternion(this.a, this.b, this.c, this.d); // TODO!!!
   }

   /**
    * Test whether the quaternion is zero.
    *
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return this.equals(new Quaternion(0, 0, 0, 0));
      //return false; // TODO!!!
   }

   /**
    * Conjugate of the quaternion. Expressed by the formula
    * conjugate(a+bi+cj+dk) = a-bi-cj-dk
    *
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(a, -b, -c, -d); // TODO!!!
   }

   /**
    * Opposite of the quaternion. Expressed by the formula
    * opposite(a+bi+cj+dk) = -a-bi-cj-dk
    *
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-a, -b, -c, -d); // TODO!!!
   }

   /**
    * Sum of quaternions. Expressed by the formula
    * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    *
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus(Quaternion q) {
      return new Quaternion(a + q.a, b + q.b, c + q.c, d + q.d); // TODO!!!
   }

   /**
    * Product of quaternions. Expressed by the formula
    * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    *
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times(Quaternion q) {
      return new Quaternion(
              a * q.a - b * q.b - c * q.c - d * q.d,
              a * q.b + b * q.a + c * q.d - d * q.c,
              a * q.c - b * q.d + c * q.a + d * q.b,
              a * q.d + b * q.c - c * q.b + d * q.a

      ); // TODO!!!
   }

   /**
    * Multiplication by a coefficient.
    *
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times(double r) {
      return new Quaternion(a * r, b * r, c * r, d * r); // TODO!!!
   }

   /**
    * Inverse of the quaternion. Expressed by the formula
    * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    *
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {

      if (this.isZero()) {
         throw new RuntimeException("Nulliga jagada ei saa");
      }
      double n = a * a + b * b + c * c + d * d;
      return new Quaternion(this.a / n, -(this.b) / n, -(this.c) / n, -(this.d) / n); // TODO!!!
   }

   /**
    * Difference of quaternions. Expressed as addition to the opposite.
    *
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus(Quaternion q) {
      return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d); // TODO!!!
   }

   /**
    * Right quotient of quaternions. Expressed as multiplication to the inverse.
    *
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight(Quaternion q) {
      if (this.isZero()) {
         throw new RuntimeException("Null numbreid olla ei tohi");
      }
      return this.times(q.inverse());
      //Quaternion guat = q.inverse();

      // return new Quaternion(this.a * guat.a, this.b * guat.b, this.c * guat.c, this.d * guat.d); // TODO!!!
   }

   /**
    * Left quotient of quaternions.
    *
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft(Quaternion q) {
      if (this.isZero()) {
         throw new RuntimeException("Null numbreid olla ei tohi");
      }
      return q.inverse().times(this);

      //Quaternion guat = q.inverse();

      //return new Quaternion(guat.a * this.a, guat.b * this.b, guat.c * this.c, guat.d * this.d);
      // TODO!!!
   }

   /**
    * Equality test of quaternions. Difference of equal numbers
    * is (close to) zero.
    *
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals(Object qo) {
      Quaternion q = (Quaternion) qo;

      if (
              Math.abs(a - q.a) < 0.0000001 &&
                      Math.abs(b - q.b) < 0.0000001 &&
                      Math.abs(c - q.c) < 0.0000001 &&
                      Math.abs(d - q.d) < 0.0000001
              ) {
         return true;
      } else {
         return false; // TODO!!!
      }

   }

   /**
    * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    *
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult(Quaternion q) {
      Quaternion temp = times(q.conjugate()).plus(q.times(conjugate()));
      return new Quaternion(temp.a/2,temp.b/2,temp.c/2,temp.d/2);

   }

   /**
    * Integer hashCode has to be the same for equal objects.
    *
    * @return hashcode
    */
   @Override
   public int hashCode() {


      return toString().hashCode(); // TODO!!!
   }

   /**
    * Norm of the quaternion. Expressed by the formula
    * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    *
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a * a + b * b + c * c + d * d); // TODO!!!
   }

   /**
    * Main method for testing purposes.
    *
    * @param arg command line parameters
    */
   public static void main(String[] arg){
   // original code
   Quaternion q = new Quaternion(1.0,1.0,1.0,1.0);
   q.valueOf(".9+.2i+.3j+.4k");

   Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
   if (arg.length > 0)
   arv1 = valueOf (arg[0]);
   System.out.println ("first: " + arv1.toString());
   System.out.println ("real: " + arv1.getRpart());
   System.out.println ("imagi: " + arv1.getIpart());
   System.out.println ("imagj: " + arv1.getJpart());
   System.out.println ("imagk: " + arv1.getKpart());
   System.out.println ("isZero: " + arv1.isZero());
   System.out.println ("conjugate: " + arv1.conjugate());
   System.out.println ("opposite: " + arv1.opposite());
   System.out.println ("hashCode: " + arv1.hashCode());
   Quaternion res = null;
   try {
      res = (Quaternion)arv1.clone();
   } catch (CloneNotSupportedException e) {};
   System.out.println ("clone equals to original: " + res.equals (arv1));
   System.out.println ("clone is not the same object: " + (res!=arv1));
   System.out.println ("hashCode: " + res.hashCode());
   res = valueOf (arv1.toString());
   System.out.println ("string conversion equals to original: "
           + res.equals (arv1));
   Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
   if (arg.length > 1)
   arv2 = valueOf (arg[1]);
   System.out.println ("second: " + arv2.toString());
   System.out.println ("hashCode: " + arv2.hashCode());
   System.out.println ("equals: " + arv1.equals (arv2));
   res = arv1.plus (arv2);
   System.out.println ("plus: " + res);
   System.out.println ("times: " + arv1.times (arv2));
   System.out.println ("minus: " + arv1.minus (arv2));
   double mm = arv1.norm();
   System.out.println ("norm: " + mm);
   System.out.println ("inverse: " + arv1.inverse());
   System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
   System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
   System.out.println ("dotMult: " + arv1.dotMult (arv2));

   System.out.println(valueOf("1+2i+4.5j+3k"));
}
}
// end of file
